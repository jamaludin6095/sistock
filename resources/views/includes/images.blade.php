@foreach( $images as $image )

<div class="ribbon-container">
	<div id="ribbon">
		@php
			$colors = explode(",", $image->colors);
			$color = $colors[0];
			if($image->extension == 'png' ) {
				//$background = 'background: url('.url('public/img/pixel.gif').') repeat center center #e4e4e4;';
			}  else {
				$background = 'background-color: #'.$color.'';
			}
			if($image->item_for_sale == 'sale') {		
				$thumbnail = 'public/uploads/preview/'.$image->preview;			
			} else {
				$thumbnail = 'public/uploads/thumbnail/'.$image->thumbnail;
			}
		@endphp
	</div>
</div>

<!-- Start Item -->
	<!-- hover-content -->
	@if( $image->item_for_sale == 'sale')
		@if( $image->item_for_sale == 'sale' ) 
			<a href="{{ url('photo', $image->id ) }}/{{str_slug($image->title)}}" class="item hovercard" data-w="{{App\Helper::getWidth($thumbnail)}}" data-h="{{App\Helper::getHeight($thumbnail)}}" style="{{$background}}">
				<img src="{{ url('public/avatar/king.png') }}" style="width:30px;height:30px;" alt="">  @endif
	@else
		<a href="{{ url('photo', $image->id ) }}/{{str_slug($image->title)}}" class="item hovercard" data-w="{{App\Helper::getWidth($thumbnail)}}" data-h="{{App\Helper::getHeight($thumbnail)}}" style="{{$background}}">
	@endif
	<span class="hover-content">
			<h5 class="text-overflow author-label mg-bottom-xs" title="{{$image->user()->username}}">
				<img src="{{ url('public/avatar/',$image->user()->avatar) }}" alt="User" class="img-circle" style="width: 20px; height: 20px; display: inline-block; margin-right: 5px;">
				<em>{{$image->user()->username}}</em>
				</h5>
				<span class="timeAgo btn-block date-color text-overflow" data="{{ date('c', strtotime( $image->date )) }}"></span>

			<span class="sub-hover">
				@if($image->item_for_sale == 'sale')
				<span class="myicon-right"><i class="fa fa-shopping-cart myicon-right"></i> {{\App\Helper::amountFormat($image->price)}}</span>
				@endif
				<span class="myicon-right"><i class="fa fa-heart-o myicon-right"></i> {{$image->likes()->count()}}</span>
				<span class="myicon-right"><i class="icon icon-Download myicon-right"></i> {{$image->downloads()->count()}}</span>
			</span><!-- Span Out -->
	</span><!-- hover-content -->
		<img src="{{ asset($thumbnail) }}" class="previewImage	" />
</a><!-- End Item -->
@endforeach	