<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Xendit\xendit;

class XenditController extends Controller
{
    private $token = 'xnd_development_5Cp1qFxH95YW0aqTecJBMfmWNg6egmPiFJv6rN8FgVqbNjqkMcX5bUSnCnEgSqdq';

    public function getlistVa()
    {
        Xendit::setApiKey($this->token);
        
        $getVABanks = \Xendit\VirtualAccounts::getVABanks();

        return response()->json([
            'data' => $getVABanks
        ])->setStatusCode(200);

    }
}
